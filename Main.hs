{-# LANGUAGE OverloadedStrings #-}
import Web.Scotty
import System.Environment (getArgs)

main :: IO ()
main =  do
  [_,port] <- getArgs
  scotty (read port) $ do 
    get "/" $ do html "hello work"
